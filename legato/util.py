import numpy as np
from sklearn.preprocessing import OneHotEncoder


LABEL_COLUMN = 'rating'

def onehot(x):
    return np.array(OneHotEncoder().fit_transform(x).todense())

def cross_columns(x_cols):
    cross_columns = dict()
    column_names = ['_'.join(crossed_column) for crossed_column in x_cols]
    for column_name, crossed_column in zip(column_names, x_cols):
        cross_columns[column_name] = crossed_column
    return cross_columns
