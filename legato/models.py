from keras import Sequential
from keras.layers import Dense, Merge, Flatten, BatchNormalization, Embedding


class Models(object):
    @staticmethod
    def embedding_model(input_shape, output_shape):
        model = Sequential()
        model.add(Embedding(input_shape, output_shape))
        model.compile(optimizer='rmsprop', loss='mse')
        return model

    @staticmethod
    def wide_deep(shape, shape_categorical, shape_embedding, y_shape):
        def wide(input_shape):
            wide = Sequential()
            wide.add(Dense(y_shape[1], input_dim=input_shape[1]))
            return wide

        def deep(deep_categorical, deep_embedding):
            def _deep_categorical(deep_categorical):
                categorical_model = Sequential()
                categorical_model.add(Dense(21, input_dim=deep_categorical[1]))
                return categorical_model

            def _deep_embedding(deep_embedding):
                embedding_model = Sequential()
                embedding_model.add(Embedding(99999, 4, input_length=3))
                embedding_model.add(Flatten())
                return embedding_model

            deep = Sequential()
            deep.add(
                Merge(
                    [_deep_categorical(deep_categorical), _deep_embedding(deep_embedding)],
                    mode='concat',
                    concat_axis=1
                )
            )
            deep.add(BatchNormalization())
            deep.add(Dense(512, activation='relu'))
            deep.add(Dense(512, activation='relu'))
            deep.add(Dense(256, activation='relu'))
            deep.add(Dense(256, activation='relu'))
            deep.add(Dense(128, activation='relu'))
            deep.add(Dense(64, activation='relu'))
            deep.add(Dense(y_shape[1], activation='sigmoid'))
            return deep

        model = Sequential()
        model.add(Merge([wide(shape), deep(shape_categorical, shape_embedding)], mode='concat', concat_axis=1))
        model.add(Dense(y_shape[1], activation='sigmoid'))

        model.compile(
            optimizer='rmsprop',
            loss='binary_crossentropy',
            metrics=['accuracy']
        )

        return model
