import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import roc_auc_score
from keras.callbacks import TensorBoard

from legato.models import Models
from legato.util import onehot, cross_columns, LABEL_COLUMN


def wide_columns_test(df, wide_column_names, cross_columns_):
    crossed_columns_dictionary = cross_columns(cross_columns_)
    categorical_columns = df.select_dtypes(include=['object']).columns.tolist()
    wide_column_names += crossed_columns_dictionary.keys()
    for key, value in crossed_columns_dictionary.items():
        df[value] = df[value].astype(str)
        df[key] = df[value].apply(lambda x: '-'.join([value[0], x[1]]) if x[0] != '0' else np.nan, axis=1)
    dummy_columns = [column for column in categorical_columns + list(crossed_columns_dictionary.keys())]
    dummy_columns.remove('genres')
    df.pop('genres')
    df = pd.get_dummies(df, columns=dummy_columns)
    df_wide = df.copy()
    df_wide.pop('userid')
    df_wide.pop('movieid')
    df_wide = MinMaxScaler().fit_transform(df_wide)
    return df_wide


def deep_columns_test(df, embedding_column_names):
    df = pd.get_dummies(df, columns=['gender'])
    df.pop('userid')
    df.pop('movieid')
    df.pop('genres')
    df_embed = df[embedding_column_names]
    for x in embedding_column_names:
        df.pop(x)
    return MinMaxScaler().fit_transform(df), MinMaxScaler().fit_transform(df_embed)


def preprocess(df, wide_column_names, embedding_column_names):
    y = onehot(df[LABEL_COLUMN].values.reshape(-1, 1))
    df = pd.concat([df, df.genres.str.get_dummies(sep='|')], axis=1)
    cross_columns_ = [
        [column, 'age'] for column in df.ix[:, 9:].columns.tolist()
    ]
    pd.set_option('display.max_columns', 500)
    df_wide = df.copy()
    df_deep = df.copy()
    df_wide = wide_columns_test(df_wide, wide_column_names, cross_columns_)
    df_deep_categorical, df_deep_embed = deep_columns_test(df_deep, embedding_column_names)

    return df_wide, df_deep_categorical, df_deep_embed, y


if __name__ == '__main__':
    wide_column_list = [
        'timestamp', 'occupation', 'zip_code'
    ]
    cross_columns_list = [
        ['genres', 'age']
    ]
    embedding_column_list = [
        'occupation', 'zip_code', 'timestamp'
    ]

    df = pd.read_csv('movielens/result.csv').head(100000)
    df['zip_code'] = df['zip_code'].map(lambda x: x.split('-')[1] if len(x.split('-')) == 2 else x)
    df.occupation = df.occupation.astype('object')
    df = df.dropna(how='any', axis=0)
    df['age'] = pd.qcut(df['age'], 2, labels=False)
    X_wide, X_deep_categorical, X_deep_embed, y = preprocess(
        df, wide_column_list, embedding_column_list
    )

    _callback = TensorBoard(
        log_dir='logs/',
        histogram_freq=0,
        batch_size=32,
        write_graph=True,
        write_grads=False,
        embeddings_freq=0
    )

    X = range(len(y))

    X_train, X_test, y_train, y_test = train_test_split(X, X, test_size=0.33, random_state=42)
    model = Models.wide_deep(X_wide.shape, X_deep_categorical.shape, X_deep_embed.shape, y.shape)
    model.fit([X_wide[X_train, :], X_deep_categorical[X_train, :], X_deep_embed[X_train, :]],
              y[y_train],
              batch_size=256,
              epochs=30,
              verbose=1,
              shuffle=True,
              callbacks=[_callback]
              )

    print(model.evaluate(
        [X_wide[X_test, :], X_deep_categorical[X_test, :], X_deep_embed[X_test, :]],
        y[y_test]
    ))

    model.save('model/wide_deep_keras.h5')

    print(
        roc_auc_score(
            y[y_test],
            model.predict([X_wide[X_test, :], X_deep_categorical[X_test, :], X_deep_embed[X_test, :]])
        )
    )
